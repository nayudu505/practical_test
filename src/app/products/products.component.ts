import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  catByProductList: any;
  cate_Id: any;
  showMessage: boolean = false;
  finalcartList = [];
  constructor(private auth_service: AuthService, private route: ActivatedRoute, private router: Router) {
    this.route.params.subscribe(res => {
      this.cate_Id = res['id'];
      console.log('categoryId', this.cate_Id);
    });
  }

  ngOnInit(): void {
    this.getCategoryByProducts();
  }

  async getCategoryByProducts() {
    let productList: any = await this.auth_service.getCategoryByProducts();
    this.catByProductList = productList.data.filter(id => id.category_id == this.cate_Id);
    console.log('get all category by product list', this.catByProductList);
  }

  addCartPage(productId) {
    console.log('added item to the cart', productId);
    let addItem:any = this.catByProductList.find((res) => res.product_id == productId);
    console.log('cart list', addItem);
   
    this.finalcartList.push(addItem);
    console.log('final list', this.finalcartList);
    let cartItems:any = this.finalcartList;
    localStorage.setItem('finalcartList', JSON.stringify(cartItems))  ;   
  }

  gotoCart() {
    this.router.navigate(['addcart'])
  }
}
