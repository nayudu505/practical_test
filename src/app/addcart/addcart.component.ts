import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addcart',
  templateUrl: './addcart.component.html',
  styleUrls: ['./addcart.component.scss']
})
export class AddcartComponent implements OnInit {
  cartItems: any;

  constructor() { }

  ngOnInit(): void {
    this.getcartItems();
  }

  getcartItems(){
    let addedItems:any = localStorage.getItem('finalcartList');
    this.cartItems = JSON.parse(addedItems);
    console.log('Product cart items: ', JSON.parse(addedItems));    
  }

}
