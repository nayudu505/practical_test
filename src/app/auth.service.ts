import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {

  }

  //get all categories

  async getAllCategories() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    })
    return await this.http.get('https://versionreview.com/arnobi_php/api/getCategories/key/73918246', { headers: headers }).toPromise();
  }

  //get gategories by product

  async getCategoryByProducts() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    })
    return await this.http.get('https://versionreview.com/arnobi_php/api/products/key/73918246', { headers: headers }).toPromise();
  }
}
