
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  categoriesList: any;
  catByProductList: any;

  constructor(private auth_service: AuthService, private router:Router) { }

  ngOnInit(): void {
    this.getCategories();
    this.getCategoryByProducts();
  }

  async getCategories() {
    let categories:any = await this.auth_service.getAllCategories();
    this.categoriesList = categories.data;
    console.log('get all categories list', this.categoriesList);
  }

  async getCategoryByProducts(){
    let productList:any = await this.auth_service.getCategoryByProducts();
    this.catByProductList = productList.data;
    console.log('get all category by product list', this.catByProductList);
  }

  gotoProductsPage(id){
    
    this.router.navigate(['products', id]);
    console.log('navigated to products page', id);
  }
}
